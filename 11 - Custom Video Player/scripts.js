/*-------------------------------3 PARTS--------------------------------*/
//GET OUR ELEMENTS
const player = document.querySelector('.player');
const video = player.querySelector('.viewer');
const progress = player.querySelector('.progress');
const progressBar = player.querySelector('.progress__filled');
const toggle = player.querySelector('.toggle');
const skipButtons = player.querySelectorAll('[data-skip]');
const ranges = player.querySelectorAll('.player__slider');
const fullscreen = player.querySelector('.fullscreen')


//BUILD OUT FUNCTIONS
function togglePlay() {
  return video.paused ? video.play() : video.pause();
}

function updatePlayButton() {
  toggle.textContent = this.paused ? '►' : '❚ ❚'
}

function skip() {
  let amountToSkip = this.dataset.skip;
  video.currentTime += Number(amountToSkip)
}

function handleRangeUpdate() {
  video[this.name] = this.value
}

function handleProgress() {
  const percent = (video.currentTime / video.duration) * 100;
  progressBar.style.flexBasis = `${percent}%`
}

function scrub(e) {
  const scrubTime = (e.offsetX / progress.offsetWidth) * video.duration;
  video.currentTime = scrubTime;
}

function scrubCursor() {
  this.style.cursor = 'grabbing';
}

function scrubCursorUp() {
  this.style.cursor = 'grab';
}


function fullScreen() {
  if (document.fullscreen) {
    document.exitFullscreen()
  }
  if (!document.fullscreen) {
    player.requestFullscreen()
  }
}

function boldHover(e) {
  e.target.style.fontWeight = 'bold';
  e.target.style.fontSize = '15px';
}

function unHover(e) {
  e.target.style.fontWeight = 'normal';
  e.target.style.fontSize = '13.333px';
}

//HOOK UP EVEN LISTENERS
video.addEventListener('click', togglePlay);
video.addEventListener('play', updatePlayButton);
video.addEventListener('pause', updatePlayButton);
video.addEventListener('timeupdate', handleProgress);

toggle.addEventListener('click', togglePlay);
toggle.addEventListener('mouseenter', boldHover)
toggle.addEventListener('mouseleave', unHover)

skipButtons.forEach((button) => {
  button.addEventListener('click', skip)
  button.addEventListener('mouseenter', boldHover)
  button.addEventListener('mouseleave', unHover)
});

ranges.forEach((range) => {
  range.addEventListener('change', handleRangeUpdate)
  range.addEventListener('mousemove', handleRangeUpdate)
  range.addEventListener('mouseenter', boldHover)
  range.addEventListener('mouseleave', unHover)
});

let mouseDown = false;
progress.addEventListener('click', scrub)
progress.addEventListener('mousemove', (e) => mouseDown && scrub(e))
progress.addEventListener('mousedown', () => mouseDown = true)
progress.addEventListener('mouseup', () => mouseDown = false)
progress.addEventListener('mousedown', scrubCursor)
progress.addEventListener('mouseup', scrubCursorUp)

fullscreen.addEventListener('click', fullScreen)
fullscreen.addEventListener('mouseenter', boldHover)
fullscreen.addEventListener('mouseleave', unHover)
